# ros-build-in-docker

## &tl;dr

```
mkdir src
cd src && git clone ...some ROS repo ...
make init
make build
```

Scripts to build LSD-SLAM-ish software in a Docker container.  Useful for 
building on non-ROS-compatible environments (Mac) or in "clean" isolated environment.

The source code to be compiled should be placed in the `src/` directory.  

These tools can be used in two different modes.

 1. In *dev* mode, - a catkin workspace is created *on the local host*.  This
 is mounted in the Docker image; and the `src/` directory is mounted as
 `workspace/src/.`   Since this workspace exists on the host machine, it is
 persistent -- build artifacts can be examined on the host machine, and
 incremental builds will work.

 However, the resulting binaries *do not* exist in the docker image.  They
 can be run by mounting the catkin workspace into a Docker image, but the
 Docker image can't be distributed.

  2. In *production* mode, the catkin workspace exists *in the docker image*.  
  The code in `src/` is copied into the Docker and built within the image.  
  This results in a new  Docker image which can be distributed _with_ the binaries.
