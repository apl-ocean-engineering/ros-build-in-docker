#!/bin/bash

if [ ! -d ~/.ssh/ ]; then
	mkdir ~/.ssh/ && touch ~/.ssh/known_hosts;
fi

if [ ! -n "$(grep "^gitlab.com " ~/.ssh/known_hosts)" ]; then
  ssh-keyscan gitlab.com  >> ~/.ssh/known_hosts 2>/dev/null;
fi
if [ ! -n "$(grep "^github.com " ~/.ssh/known_hosts)" ]; then
  ssh-keyscan github.com  >> ~/.ssh/known_hosts 2>/dev/null;
fi

WSTOOL_RECURSIVE="true"

subcommand=$1
case $subcommand in
    "init" )
	if [ ! -f "src/.rosinstall" ]; then
        	wstool init src;
	fi

        while : ; do
          rosinstall_files=$(find src -type f -name "*?.rosinstall")
          rosinstall_file=$(echo ${rosinstall_files} | cut -d ' ' -f 1)
          if [[ -z "${rosinstall_file}" ]]; then
            break
          fi

          wstool merge ${rosinstall_file} -t src -y

          if [[ "${WSTOOL_RECURSIVE}" != "true" ]]; then break; fi

          rm ${rosinstall_file}
          wstool update -t src
        done

        ;&
    "rosdep" )
        # use rosdep to install dependencies
        rosdep update
        . /opt/ros/${ROS_RELEASE}/setup.sh
        rosdep install --from-paths src --ignore-src -r -y
        ;&
    "config" )
        catkin config --install --extend /opt/ros/${ROS_RELEASE} --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac
