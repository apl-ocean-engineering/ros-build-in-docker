#!/usr/bin/env bash

BUILD_WORKSPACE=${BUILD_WORKSPACE:-/home/ros/workspace}

source ${BUILD_WORKSPACE}/install/setup.bash

sleep 1

exec $*
