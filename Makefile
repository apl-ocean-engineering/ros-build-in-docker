
# Set to anything non-zero for verbose build.
VERBOSE ?=


## Volumes used by all Docker versions
HOME_IN_DOCKER=/home/ros
DOCKER_WORKING_ROOT=${HOME_IN_DOCKER}/workspace

CATKIN_ARGS ?=
DOCKER_RUN_ARGS=--rm -t -w ${DOCKER_WORKING_ROOT} -e BUILD_WORKSPACE=${DOCKER_WORKING_ROOT}

ifneq ($(VERBOSE),)
	CATKIN_ARGS += --verbose
	DOCKER_RUN_ARGS += -e VERBOSE=1
endif


default: help

help:
	@echo "make init         Initialize the src/ directory with the repositories described in lsd-slam-ros-repos"
	@echo "make build        Run \"catkin build\""
	@echo "make clean        Run \"catkin clean\""
	@echo "make status       Run \"status\" on each src/ directory"
	@echo "make update       Run \"update\" on each src/ directory"
	@echo "make bash         Run bash shell in Docker image"
	@echo ""
	@echo "make dev_build_image    Build Docker build image"
	@echo
	@echo "make prod         Build \"production\" Docker image"
	@echo "Set environment variable VERBOSE=TRUE for verbose output."



##== ==

UBUNTU_RELEASE=bionic
ROS_RELEASE=melodic

DEV_BUILD_IMAGE=amarburg/ros-build-env-${UBUNTU_RELEASE}-${ROS_RELEASE}:latest
PROD_IMAGE?=amarburg/ros-build-${UBUNTU_RELEASE}-${ROS_RELEASE}-prod:latest

LOCAL_SRC=src
LOCAL_CATKIN_WS=${UBUNTU_RELEASE}-${ROS_RELEASE}-ws

DOCKER_HOME=/root
DOCKER_WS=${DOCKER_HOME}/workspace

DOCKER_VOLUMES= -v "${PWD}/dot-ros:${DOCKER_HOME}/.ros" \
								-v "${LOCAL_CATKIN_WS}:${DOCKER_WS}" \
								-v "${LOCAL_SRC}:${DOCKER_WS}/src" \
								-v "${PWD}/ssh:${DOCKER_HOME}/.ssh"

DOCKER_ENV= -e DOCKER_WS=${DOCKER_WS} -e ROS_RELEASE=${ROS_RELEASE}

DOCKER_RUN_ARGS= --rm -it

DOCKER_RUN_DEV_BUILD = docker run ${DOCKER_RUN_ARGS} ${DOCKER_ENV} ${DOCKER_VOLUMES} --workdir ${DOCKER_WS} ${DEV_BUILD_IMAGE}

WSTOOL_ARGS=-t src/

init: dirs dev_build_image
	${DOCKER_RUN_DEV_BUILD} ${DOCKER_HOME}/scripts/ws_setup.sh init

update: dirs
	${DOCKER_RUN_DEV_BUILD} wstool ${WSTOOL_ARGS} update

status: dirs
	${DOCKER_RUN_DEV_BUILD} wstool ${WSTOOL_ARGS} status

build: dirs
	${DOCKER_RUN_DEV_BUILD} catkin build ${CATKIN_ARGS}

test: dirs
	${DOCKER_RUN_DEV_BUILD} catkin build ${CATKIN_ARGS} --catkin-make-args run_tests && \
  for d in ${XENIAL_SRCS}/; do  \
          ${DOCKER_RUN_DEV_BUILD}  catkin_test_results "build/$d"; \
  done

clean: dirs
	${DOCKER_RUN_DEV_BUILD} catkin clean --yes

bash: dirs
	docker run ${DOCKER_RUN_ARGS} ${DOCKER_VOLUMES} --rm -it ${DEV_BUILD_IMAGE} bash

dirs: ${LOCAL_CATKIN_WS} ${LOCAL_SRC}
	mkdir -p dot-ros
	mkdir -p ssh

${LOCAL_CATKIN_WS}:
	mkdir -p $@/logs $@/scripts $@/src $@/install

${LOCAL_SRC}:
	mkdir -p $@

##== ==

dev_build_image:
		docker build -t ${DEV_BUILD_IMAGE} -f Dockerfiles/Dockerfile_dev_build_image \
		 	--build-arg UBUNTU_RELEASE=${UBUNTU_RELEASE} \
			--build-arg ROS_RELEASE=${ROS_RELEASE} \
			--build-arg DOCKER_WS=${DOCKER_WS} \
			--build-arg DOCKER_HOME=${DOCKER_HOME} .

##== Prod ==

prod:
	docker build -t ${PROD_IMAGE} -f Dockerfiles/Dockerfile_prod --progress auto --no-cache \
			--build-arg ROS_RELEASE=${ROS_RELEASE} \
			--build-arg DEV_BUILD_IMAGE=${DEV_BUILD_IMAGE} \
			--build-arg LOCAL_CATKIN_WS=${LOCAL_CATKIN_WS} \
			--build-arg LOCAL_SRC=${LOCAL_SRC}  \
			--build-arg DOCKER_WS=${DOCKER_WS}  \
			--build-arg DOCKER_HOME=${DOCKER_HOME} .

.PHONY:  init pull config status build clean bash dev_build_image dirs
